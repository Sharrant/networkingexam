NetworkingExam

Clients enter a username and rating upon joining
game servers automatically generate an IP and game load

After signing into the matchmaking server, pressing enter on the client toggles you between in queue and out of queue.

The program will crash if a client or game server is taken offline.


There's an issue where sometimes the game client registers twice. Restarting the program usually fixes it.