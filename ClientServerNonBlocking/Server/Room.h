#ifndef __ROOM_H
#define __ROOM_H

#include <vector>
class Room {
public:
	Room(int roomID);
	~Room();

	void JoinRoom(int connID);
	void LeaveRoom(int connID);
	bool Contains(int connID);

	int connections[20];
	int roomID;
	int numofConnections = 0;
};


#endif
