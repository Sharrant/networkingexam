#include "Room.h"


Room::Room(int roomID)
{
	this->roomID = roomID;
}

Room::~Room()
{

}

void Room::JoinRoom(int connID)
{
	for (int i = 0; i < numofConnections; i++)
	{
		if (connections[i] == connID)
		{
			break;
		}
	}
	connections[numofConnections] = connID;
	numofConnections++;
}

void Room::LeaveRoom(int connID)
{
	int removedConn = 0;
	for (int i = 0; i < numofConnections; i++)
	{
		if (connections[i] == connID)
		{
			removedConn = i;
		}
	}

	for (int i = removedConn; i < numofConnections-1; i++)
	{
		connections[i] = connections[i + 1];
	}
	numofConnections--;
}

bool Room::Contains(int connID)
{
	for (int i = 0; i < numofConnections; i++)
	{
		if (connections[i] == connID)
		{
			return true;
		}
	}
	return false;
}