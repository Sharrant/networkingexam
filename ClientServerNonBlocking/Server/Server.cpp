#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>
#include <iostream>
#include <thread>
#include <time.h>
#include "..\common\Buffer.h"
#include "..\common\Protocols.pb.h"
#include "Room.h"

// for windows socket link
#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_PORT "27015"
#define DEFAULT_BUFLEN 512


void ReadIncomingMessage(Buffer* receiveBuffer);
void ComposeMessage(std::string userName1, std::string userName2, std::string ipAddress, int messageID, Buffer* sendBuffer);

struct sUnderwatchServer
{
	std::string ipAddress;
	int numPlayers;
};

struct sUnderwatchPlayer
{
	std::string userName;
	int playerRating;
	int queueRange;
	int socket;
};

std::vector<sUnderwatchServer> gameServerList;
std::vector<sUnderwatchPlayer> playerList;
time_t start;
bool MatchRatings(sUnderwatchPlayer player1, sUnderwatchPlayer player2);
void MatchMaker();
void MatchFound(sUnderwatchPlayer player1, sUnderwatchPlayer player2);
fd_set master;    //Master list
fd_set read_fds;
SOCKET listeningSocket;

int main() 
{

	//Step 1
	//Initial winsock
	WSADATA wsaData; //Create a Windows Socket Application Data Object

	int resultInt;

	resultInt = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (resultInt != 0)
	{
		std::cout << "WinSock Initalization failed" << std::endl;
		return 1;
	}

	//Step 2
	//Create a Socket
	struct addrinfo *result = NULL;
	struct addrinfo *ptr = NULL;
	struct addrinfo hints;

	ZeroMemory(&hints, sizeof(hints)); //Fills a block of memory with zeros
	hints.ai_family = AF_INET; //Unspecified so either IPv4 or IPv6 address can be returned
	hints.ai_socktype = SOCK_STREAM; //Stream must be specified for TCP
	hints.ai_protocol = IPPROTO_TCP; //Protocol is TCP
	hints.ai_flags = AI_PASSIVE;


	resultInt = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (resultInt != 0)
	{
		std::cout << "Socket Initalization failed" << std::endl;
		WSACleanup(); //will nicely kill our WinSock instance
		return 1;
	}

	std::cout << "Step 1: WinSock Initalized" << std::endl;

	//Copy the result object pointer
	ptr = result;

	listeningSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

	if (listeningSocket == INVALID_SOCKET)
	{
		std::cout << "Socket Initalization failed" << std::endl;
		freeaddrinfo(result); //free memory allocated to provent memory leaks
		WSACleanup(); //will nicely kill our WinSock instance
		return 1;
	}
	srand(time(0));


	/*NEW STUFF OCT 3RD */

	//We need to switch the socket to be non blocking
	// If iMode!=0, non-blocking mode is enabled.
	u_long iMode = 1;
	ioctlsocket(listeningSocket, FIONBIO, &iMode);

	/****/


	std::cout << "Step 2: Socket Created" << std::endl;

	//Step3 
	//Bind the Socket

	resultInt = bind(listeningSocket, result->ai_addr, (int)result->ai_addrlen);

	if (listeningSocket == INVALID_SOCKET)
	{
		std::cout << "Socket binding failed" << std::endl;
		freeaddrinfo(result); //free memory allocated to provent memory leaks
		closesocket(listeningSocket); //Close the socket
		WSACleanup(); //will nicely kill our WinSock instance
		return 1;
	}

	freeaddrinfo(result); //Once bind is called the address info is no longer needed so free the memory allocated

	std::cout << "Step 3: Socket Bound" << std::endl;

	//Step 4
	//Listen for a client connection

	if (listen(listeningSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		std::cout << "Socket listening failed" << std::endl;
		closesocket(listeningSocket); //Close the socket
		WSACleanup(); //will nicely kill our WinSock instance
		return 1;
	}

	std::cout << "Step 4: Listening on Socket" << std::endl;


	/*NEW STUFF OCT 3RD */

	//Set up file descriptors for select statement
  //Temp list for select() statement

					  //Clear the master and temp sets
	FD_ZERO(&master);
	FD_ZERO(&read_fds);

	//Add the listener to the master set
	FD_SET(listeningSocket, &master);

	//Keep track of the biggest file descriptor
	int newfd;        //Newly accepted socket descriptor
	int fdmax = listeningSocket; //Maximum file descriptor number (to start it's the listener)
	std::cout << "fdmax " << fdmax << std::endl;

	//Set timeout time
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 500 * 1000; // 500 ms
	bool authServerAttached = false;
	int authServerNumber = 0;
	std::thread MatchmakerThread(MatchMaker);
	//Main loop
	start = time(0);
	while (true) //infinite loop, this could be scary
	{

		//Select function checks to see if any socket has activity
		read_fds = master; // copy the master to a temp
		if (select(fdmax + 1, &read_fds, NULL, NULL, &tv) == -1)
		{
			std::cout << "Select Error" << std::endl;
			exit(4);
		}

		//Loop through existing connections looking for data to read
		for (int i = 0; i <= fdmax; i++)
		{
			//If no flag is raised keep looping
			if (!FD_ISSET(i, &read_fds))
			{
				continue;
			}

			//If the raised flag is on the listening socket accept a new connection then keep looping
			if (i == listeningSocket)
			{

				// handle new connections
				sockaddr_in client;
				socklen_t addrlen = sizeof sockaddr_storage; //this represents the client address size
				newfd = accept(listeningSocket, (struct sockaddr *)&client, &addrlen);
				std::cout << "Connected to: " << client.sin_addr.s_addr << std::endl;

				std::cout << "newfd " << newfd << std::endl;
				if (newfd == -1)
				{
					std::cout << "Accept Error" << std::endl;
					continue;
				}

				FD_SET(newfd, &master); // add to master set

				//Keep track of max fd
				if (newfd > fdmax)
					fdmax = newfd;

				std::cout << "New connection on socket " << newfd << std::endl;
				continue;
			}

			/////////////////////////////////
			// Recieve an incoming message //
			/////////////////////////////////
			Buffer* receiveBuffer = new Buffer(DEFAULT_BUFLEN);
			char recvbuf[DEFAULT_BUFLEN];
		/*	for (int i = 0; i < DEFAULT_BUFLEN;i++)
			recvbuf[i] = { 0 };*/
			int incomingMessageLength = 0;
			
			//Recieve the message
			//ZeroMemory(recvbuf, 512);
			incomingMessageLength = recv(i, recvbuf, sizeof recvbuf, 0);
			
			if (incomingMessageLength > 0)
			{
				std::cout << "Bytes received: " << incomingMessageLength << std::endl;

				//std::cout << "Recieved Message: " << recvbuf << std::endl;
				receiveBuffer->clear();
				


				receiveBuffer->loadBuffer(recvbuf, incomingMessageLength);
				
				int packetlength, messageID, commandID, roomID, messageLength = 0;
				packetlength = receiveBuffer->readShort16BE();

				messageID = receiveBuffer->readIntBE();
				if (messageID==-1)
				{
					
					sUnderwatchServer newServer;// = new sUnderwatchServer();
					newServer.numPlayers  = receiveBuffer->readIntBE();
					int read = (packetlength - receiveBuffer->getReadIndex())-2;
					newServer.ipAddress = receiveBuffer->readStringBE(receiveBuffer->getReadIndex()+2,read);
					std::cout << newServer.numPlayers << " : " << newServer.ipAddress << std::endl;
					
					gameServerList.push_back(newServer);

					////if (chatPacket.email() == authName && chatPacket.plaintextpassword() == authPass)
					//{
					//	authServerNumber = i;
					//	authServerAttached = true;
					//	receiveBuffer = new Buffer(DEFAULT_BUFLEN);
					//	continue;
					//}

				}
				else if (messageID==1)
				{
					//packetlength = receiveBuffer->returnBufferSize();
					int read = receiveBuffer->readShort16BE();
					std::string userName = receiveBuffer->readStringBE(read);
					int skillRating = receiveBuffer->readIntBE();
					sUnderwatchPlayer newPlayer;
					newPlayer.userName = userName;
					newPlayer.playerRating = skillRating;
					newPlayer.queueRange = 100;
					newPlayer.socket = i;
					std::cout << newPlayer.userName << " : " << newPlayer.playerRating << std::endl;
					playerList.push_back(newPlayer);
					std::cout << "Player List Size: " << playerList.size() << std::endl;
						//	messageID = receiveBuffer->readIntBE();
					//if (packetlength > 0)
					//{
					//	//ReadIncomingMessage(receiveBuffer);


					//	for (int unsigned j = 0; j < master.fd_count; j++)
					//	{
					//		if (master.fd_array[j] != listeningSocket)
					//		{
					//			//	if (i != j) {
					//			int jSendResult = send(master.fd_array[j], receiveBuffer->toCharArray(), receiveBuffer->returnBufferSize(), 0);

					//			if (jSendResult == SOCKET_ERROR)
					//			{
					//				std::cout << "Send failed: " << WSAGetLastError() << std::endl;
					//				closesocket(i);
					//				WSACleanup();
					//				return 1;
					//			}
					//			std::cout << "Bytes sent: " << jSendResult << std::endl;
					//			//	}
					//		}

					//	}
					//}

				}
				else if (messageID == 2)
				{

					//packetlength = receiveBuffer->returnBufferSize();
					int read = receiveBuffer->readShort16BE();
					std::string userName = receiveBuffer->readStringBE(read);
					int skillRating = receiveBuffer->readIntBE();
					//sUnderwatchPlayer newPlayer;
					//newPlayer.userName = userName;
					//newPlayer.playerRating = skillRating;
					//newPlayer.queueRange = 100;
					//newPlayer.queueTime = 0;
				//	std::cout << newPlayer.userName << " : " << newPlayer.playerRating << std::endl;
				//	playerList.push_back(newPlayer);
					int size = playerList.size();
					for (unsigned int i = 0; i < size; i++)
					{
						if (playerList[i].userName == userName)
						{
							playerList.erase(playerList.begin()+i);
						}
					}
					std::cout << "Player List Size: " << playerList.size() << std::endl;
				}


				/*if (iSendResult == SOCKET_ERROR)
				{
					std::cout << "Send failed: " << WSAGetLastError() << std::endl;
					closesocket(i);
					WSACleanup();
					return 1;
				}*/
			//	receiveBuffer->loadBuffer(recvbuf, incomingMessageLength);
			////	std::cout << receiveBuffer->readStringBE(incomingMessageLength);
			//	ReadIncomingMessage(receiveBuffer);
			//	std::cout << "Bytes sent: " << iSendResult << std::endl;
				/*receiveBuffer->loadBuffer(recvbuf, incomingMessageLength);
				std::cout << receiveBuffer->readStringBE(incomingMessageLength);
				std::cout << "\n";*/
			}
			else if (incomingMessageLength == 0)
			{
				std::cout << "Connection closing..." << std::endl;
				closesocket(i); //CLose the socket
				FD_CLR(i, &master); // remove from master set

				continue;
			}
			else
			{
				std::cout << "recv failed: " << WSAGetLastError() << std::endl;
				closesocket(i);
				WSACleanup();

				//You probably don't want to stop the server based on this in the real world
				return 1;
			}
		}
	}

	/****/


	//Step 7
	//Disconnect and Cleanup

	// shutdown the send half of the connection since no more data will be sent
	resultInt = shutdown(listeningSocket, SD_SEND);
	if (resultInt == SOCKET_ERROR)
	{
		std::cout << "shutdown failed:" << WSAGetLastError << std::endl;
		closesocket(listeningSocket);
		WSACleanup();
		return 1;
	}

	//Final clean up
	closesocket(listeningSocket);
	WSACleanup(); 

	std::cout << "Step 7: Disconnect" << std::endl;

	//Keep the window open
	std::cout << "\nwaiting on exit";
	int tempInput;
	std::cin >> tempInput;

	return 0;
}

void ReadIncomingMessage(Buffer* receiveBuffer)
{
	int packetlength, messageID, roomID = 0;
	long messageLength = 0;
	std::string message;
	std::string username;
	receiveBuffer->setReadIndex(0);
	//std::string serializedChatPacket = receiveBuffer->readStringBE(52);*/
	packetlength = receiveBuffer->readShort16BE();
	receiveBuffer->setReadIndex(6);
	
	std::string serializedChatPacket;
	int messageLen = packetlength - 2;
	serializedChatPacket = receiveBuffer->readStringBE(4,messageLen);//(packetlength -2);




	chatpacket::AuthenticateWeb chatPacket;
	chatPacket.ParseFromString(serializedChatPacket);
//	chatPacket.
	std::string serializedWebAuth;


	//std::cout << createWeb.email() << " " << createWeb.requestid() << " " << createWeb.plaintextpassword << std::endl;
	//messageID = receiveBuffer->readIntBE();
//	messageID = chatPacket.messageid();
//	//roomID = receiveBuffer->readIntBE();
//	//messageLength = receiveBuffer->readIntBE();
//	message = chatPacket.message();
//	messageLength = message.length();
//	//receiveBuffer->addToReadIndex(2);
//	//message = receiveBuffer->readStringBE(messageLength-2);
//	username = chatPacket.username();
//
//
////	std::cout << "packet length: " << packetlength << std::endl;
//	std::cout << "message id: " << chatPacket.messageid() << std::endl;
////	std::cout << "roomID: " << roomID << std::endl;
//	std::cout << "message length: " << message.length() << std::endl;
//	std::cout << "username: " << chatPacket.username() << std::endl;
//	std::cout << "message: " << chatPacket.message() << std::endl;
}

void HandleMessage(Buffer* receiveBuffer)
{
	int packetlength, messageID,commandID, roomID, messageLength = 0;
	std::string message;
	receiveBuffer->setReadIndex(0);
	packetlength = receiveBuffer->readShort16BE();
	messageID = receiveBuffer->readIntBE();
	if (messageID == -1)
	{
		commandID = receiveBuffer->readIntBE();
		{
			if (commandID = -1)
			{
				roomID = receiveBuffer->readIntBE();
				receiveBuffer->addToReadIndex(2);
				message = receiveBuffer->readStringBE(messageLength - 14) + " has joined the room.";
			}
			else if (commandID = -2)
			{
				roomID = receiveBuffer->readIntBE();
				receiveBuffer->addToReadIndex(2);
				message = receiveBuffer->readStringBE(messageLength - 14) + " has left the room.";
			}
		}
	}
}

void ComposeMessage(std::string userName1, std::string userName2, std::string ipAddress, int messageID, Buffer* sendBuffer)
{
	
	sendBuffer->clear();
	sendBuffer->writeShort16BE(0);
	sendBuffer->setWriteIndex(2);
	sendBuffer->writeIntBE(1);
	sendBuffer->writeStringBE(userName1);
	sendBuffer->writeStringBE(userName2);
	sendBuffer->writeStringBE(ipAddress);
	sendBuffer->setWriteIndex(0);
	sendBuffer->writeShort16BE(sendBuffer->returnBufferSize());

	//sendBuffer->writeIntBE(messageID);
	//sendBuffer->writeIntBE(0);
	//sendBuffer->writeIntBE(4 + userName.length() + message.length());
	//sendBuffer->writeStringBE(userName + message);
}

void ReceiveMessages(SOCKET ConnectSocket, Buffer* sendBuffer, Buffer* receiveBuffer, char recvbuf[], int recvbuflen)
{
	do
	{
		sendBuffer->setReadIndex(0);
		sendBuffer->setWriteIndex(0);
		int resultInt = 0;
		resultInt = recv(ConnectSocket, recvbuf, recvbuflen, 0);


		if (resultInt > 0)
		{
			receiveBuffer->clear();
			//std::cout << "Bytes received: " << resultInt << std::endl;
			receiveBuffer->loadBuffer(recvbuf, resultInt);
			/*receiveBuffer->setReadIndex(0);
			receiveBuffer->setWriteIndex(0);
			std::string check1 = receiveBuffer->toCharArray();
			std::string check2 = sendBuffer->toCharArray();
			int checkMessageId1 = receiveBuffer->readIntBE(2);
			int checkMessageId2 = sendBuffer->readIntBE(2);
			if (check1 != check2 && checkMessageId1!= checkMessageId2)*/
			//std::cout << receiveBuffer->readStringBE(resultInt);
			ReadIncomingMessage(receiveBuffer);
		}
		else if (resultInt == 0)
		{
			//	std::cout << "Connection closed" << std::endl;

		}
		else
			std::cout << "recv failed with error: " << WSAGetLastError() << std::endl;

		
	} while (true);
}

void MatchMaker()
{
	while (true)
	{
		int size = playerList.size();
		// std::cout << "Player List Size: " << playerList.size() << std::endl;
		for (unsigned int i = 0; i < size; i++)
		{
			for (unsigned int j = i + 1; j < size; j++)
			{
				if (MatchRatings(playerList[i], playerList[j]))
				{
					std::cout << "WE HAVE A MATCH!" << std::endl;
					MatchFound(playerList[i], playerList[j]);
					playerList[i].queueRange = 100;
					playerList[j].queueRange = 100;
					
				}
			}
		}
		
		double seconds_since_start = difftime(time(0), start);
		if (seconds_since_start > 30.f)
		{
			
			for (unsigned int i = 0; i < size; i++)
			{
				playerList[i].queueRange += 10;
				if (playerList[i].queueRange > 500)
				{
					playerList[i].queueRange = 500;
				}
				std::cout << playerList[i].userName << " Queue Range :" << playerList[i].queueRange << std::endl;
			}
			start = time(0);
		}
	}
}

bool MatchRatings(sUnderwatchPlayer player1, sUnderwatchPlayer player2)
{
	if ((player1.playerRating+player1.queueRange > player2.playerRating && (player1.playerRating-player1.queueRange < player2.playerRating)))
	{

	}
	else return false;

	if ((player2.playerRating + player2.queueRange > player1.playerRating && (player2.playerRating - player2.queueRange < player1.playerRating)))
	{

	}
	else return false;


	return true;
}

void MatchFound(sUnderwatchPlayer player1, sUnderwatchPlayer player2)
{
	Buffer* sendBuffer = new Buffer(DEFAULT_BUFLEN);
	sendBuffer->clear();
	std::string ipAddress = "";
	int size = gameServerList.size();
	int lowestPop;
	if (!gameServerList.empty())
	lowestPop = gameServerList[0].numPlayers;
	ipAddress = gameServerList[0].ipAddress;
	int count = 0;
	for (unsigned int i = 1; i < size; i++)
	{
		if (lowestPop > gameServerList[i].numPlayers)
		{
			lowestPop = gameServerList[i].numPlayers;
			ipAddress = gameServerList[i].ipAddress;
			count = i;
		}
	}
	ComposeMessage(player1.userName, player2.userName, ipAddress, 1, sendBuffer);
	gameServerList[count].numPlayers++;
	for (int unsigned j = 0; j < master.fd_count; j++)
	{
		if (master.fd_array[j] == player1.socket)
		{
			//	if (i != j) {
			int jSendResult = send(master.fd_array[j], sendBuffer->toCharArray(), sendBuffer->returnBufferSize(), 0);

			if (jSendResult == SOCKET_ERROR)
			{
				std::cout << "Send failed: " << WSAGetLastError() << std::endl;
				//closesocket(i);
				//WSACleanup();
				//return 1;
			}
			std::cout << "Bytes sent: " << jSendResult << std::endl;
			//	}
		}

	}
	ComposeMessage(player2.userName, player1.userName, ipAddress, 1, sendBuffer);
	for (int unsigned j = 0; j < master.fd_count; j++)
	{
		if (master.fd_array[j] == player2.socket)
		{
			//	if (i != j) {
			int jSendResult = send(master.fd_array[j], sendBuffer->toCharArray(), sendBuffer->returnBufferSize(), 0);

			if (jSendResult == SOCKET_ERROR)
			{
				std::cout << "Send failed: " << WSAGetLastError() << std::endl;
				//closesocket(i);
				//WSACleanup();
				//return 1;
			}
			std::cout << "Bytes sent: " << jSendResult << std::endl;
			//	}
		}

	}

}