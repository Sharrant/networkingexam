#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>

//
//#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
//#include <iphlpapi.h>
//#include <stdlib.h>
////#include <stdio.h>
//#include <iostream>
//#include <cstdlib>
//#include <cstring>
#include <fstream>
//#include <string>
#include <thread>

#include "..\common\Buffer.h"


// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")
#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"


//void ReceiveMessages(SOCKET ConnectSocket, Buffer* sendBuffer, Buffer* receiveBuffer, char recvbuf[], int recvbuflen);
std::string gen_randomIP();
//void ReadIncomingMessage(Buffer* receiveBuffer);
void ComposeMessage(std::string userName, int playersConnected, std::string ipAddress, int messageID, Buffer* sendBuffer);
//This function will be called for every row of data returned in the select statement




int __cdecl main(int argc, char **argv)
{

	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL;
	struct addrinfo *ptr = NULL;
	struct addrinfo hints;

	Buffer* sendBuffer = new Buffer(DEFAULT_BUFLEN);
	Buffer* receiveBuffer = new Buffer(DEFAULT_BUFLEN);
	//Create a database pointer
	//sqlite3 *database;
	char *errorMessage = 0;


	//Attempt to open the database






	char recvbuf[DEFAULT_BUFLEN];
	int resultInt;
	int recvbuflen = DEFAULT_BUFLEN;

	//Step 1
	// Initialize Winsock
	resultInt = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (resultInt != 0)
	{
		std::cout << "WinSock Initalization failed" << std::endl;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	resultInt = getaddrinfo("127.0.0.1", DEFAULT_PORT, &hints, &result);
	if (resultInt != 0) {
		std::cout << "Socket Initalization failed" << std::endl;
		WSACleanup();
		return 1;
	}

	//std::cout << "Step 1: WinSock Initalized" << std::endl;

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next)
	{

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

		if (ConnectSocket == INVALID_SOCKET)
		{
			std::cout << "Socket failed with error: " << WSAGetLastError() << std::endl;
			WSACleanup();
			return 1;
		}

		// Connect to server.
		resultInt = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);

		if (resultInt == SOCKET_ERROR)
		{
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET)
	{
		std::cout << "Unable to connect to server!" << std::endl;
		WSACleanup();
		return 1;
	}
	srand(time(0));
	ComposeMessage("GameServer",(rand() % 400+101), gen_randomIP(), -1, sendBuffer);

	// Send an initial buffer
	//ReadIncomingMessage(sendBuffer);
	const char* temp = sendBuffer->toCharArray();
	//	std::cout << sendBuffer->readStringBE(0,sendBuffer->returnBufferSize()) <<std::endl;
	resultInt = send(ConnectSocket, sendBuffer->toCharArray(), sendBuffer->returnBufferSize(), 0);
	if (resultInt == SOCKET_ERROR)
	{
		std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}

	//std::cout << "Step 4: Sending / Receiving data" << std::endl;

	//std::cout << "Bytes Sent: " << resultInt << std::endl;


	// shutdown the connection since no more data will be sent

	std::string tempInput;

	//std::thread receiveThread(ReceiveMessages, ConnectSocket, sendBuffer, receiveBuffer, recvbuf, recvbuflen);
	resultInt = send(ConnectSocket, sendBuffer->toCharArray(), sendBuffer->returnBufferSize(), 0);
	do
	{
		/*If message is in queue
		Send message to Server*/


		//sendbuf = &tempInput[0u];
	//	resultInt = send(ConnectSocket, sendBuffer->toCharArray(), sendBuffer->returnBufferSize(), 0);
		if (resultInt == SOCKET_ERROR)
		{
			std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
			closesocket(ConnectSocket);
			WSACleanup();
			return 1;
		}
	} while (resultInt > 0);


	resultInt = shutdown(ConnectSocket, SD_SEND);
	if (resultInt == SOCKET_ERROR)
	{
		std::cout << "shutdown failed with error: " << WSAGetLastError() << std::endl;
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}
	closesocket(ConnectSocket);
	WSACleanup();

	//std::cout << "Step 5: Disconnect" << std::endl;
	return 0;

	//////////////////////
	/* Create Statement */
	//////////////////////

	// Create SQL statement



	////////////////////////
	///* Insert Statement */
	////////////////////////



}


//
//

std::string gen_randomIP() {
	srand(time(0));
	std::stringstream ss;
	ss << (rand() % 255 + 1) << ".";
	//srand(time(NULL));
	ss << (rand() % 255 + 1) << ".";
//	srand(time(NULL));
	ss << (rand() % 255 + 1) << ".";
//	srand(time(NULL));
	ss << (rand() % 255 + 1);
	return ss.str();
}
//
//
//void ReceiveMessages(SOCKET ConnectSocket, Buffer* sendBuffer, Buffer* receiveBuffer, char recvbuf[], int recvbuflen)
//{
//	do
//	{
//		sendBuffer->setReadIndex(0);
//		sendBuffer->setWriteIndex(0);
//		int resultInt = 0;
//		resultInt = recv(ConnectSocket, recvbuf, recvbuflen, 0);
//
//
//		if (resultInt > 0)
//		{
//			receiveBuffer->clear();
//			//std::cout << "Bytes received: " << resultInt << std::endl;
//			receiveBuffer->loadBuffer(recvbuf, resultInt);
//			/*receiveBuffer->setReadIndex(0);
//			receiveBuffer->setWriteIndex(0);
//			std::string check1 = receiveBuffer->toCharArray();
//			std::string check2 = sendBuffer->toCharArray();
//			int checkMessageId1 = receiveBuffer->readIntBE(2);
//			int checkMessageId2 = sendBuffer->readIntBE(2);
//			if (check1 != check2 && checkMessageId1!= checkMessageId2)*/
//			//std::cout << receiveBuffer->readStringBE(resultInt);
//			ReadIncomingMessage(receiveBuffer);
//		}
//		else if (resultInt == 0)
//		{
//			//	std::cout << "Connection closed" << std::endl;
//
//		}
//		else
//			std::cout << "recv failed with error: " << WSAGetLastError() << std::endl;
//
//
//	} while (true);
//}
//
//
//void ReadIncomingMessage(Buffer* receiveBuffer)
//{
//
//	int packetlength, messageID, roomID = 0;
//	long messageLength = 0;
//	std::string message;
//	std::string username;
//	receiveBuffer->setReadIndex(0);
//	//std::string serializedChatPacket = receiveBuffer->readStringBE(52);*/
//	packetlength = receiveBuffer->readShort16BE();
//	receiveBuffer->setReadIndex(6);
//
//	std::string serializedChatPacket;
//	int messageLen = packetlength - 4;
//	//int messageType = receiveBuffer->readIntBE()
//	serializedChatPacket = receiveBuffer->readStringBE(4, messageLen);//(packetlength -2);
//
//	chatpacket::AuthenticateWeb chatPacket;
//	chatPacket.ParseFromString(serializedChatPacket);
//	//	chatPacket.
//	int test = chatPacket.requestid();
//	if (chatPacket.requestid() == 1)
//	{
//
//	//	AuthenticateUser(chatPacket.email(), chatPacket.plaintextpassword());
//		std::cout << authUserID <<  " " << authCreationDate << std::endl;
//		
//	}
//	else// if (chatPacket.requestid() == 2)
//	{
//		chatpacket::CreateWebAccount chatPacket2;
//		chatPacket2.ParseFromString(serializedChatPacket);
//		//RegisterUser(chatPacket2.email(), chatPacket2.plaintextpassword());
//	}
//	
//
//}


void ComposeMessage(std::string userName, int playersConnected, std::string ipAddress, int messageID, Buffer* sendBuffer)
{



	sendBuffer->clear();
	sendBuffer->writeShort16BE(0);
	sendBuffer->setWriteIndex(2);
	sendBuffer->writeIntBE(messageID);
	sendBuffer->writeIntBE(playersConnected);
	sendBuffer->writeStringBE(ipAddress);
	sendBuffer->setWriteIndex(0);
	sendBuffer->writeShort16BE(sendBuffer->returnBufferSize());

	//sendBuffer->writeIntBE(messageID);
	//sendBuffer->writeIntBE(0);
	//sendBuffer->writeIntBE(4 + userName.length() + message.length());
	//sendBuffer->writeStringBE(userName + message);
}
