#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <string>
#include <thread>

#include "..\common\Buffer.h"
//#include "..\common\Protocols.pb.h"


// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

void ComposeMessage(std::string userName,int skillRating, int mode, int messageID, Buffer* sendBuffer);
void ReadIncomingMessage(Buffer* receiveBuffer);
void ReceiveMessages(SOCKET ConnectSocket, Buffer* sendBuffer, Buffer* receiveBuffer, char recvbuf[], int recvbuflen);
int __cdecl main(int argc, char **argv) 
{
    WSADATA wsaData;
    SOCKET ConnectSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL;
    struct addrinfo *ptr = NULL;
    struct addrinfo hints;

	Buffer* sendBuffer = new Buffer(DEFAULT_BUFLEN);
	Buffer* receiveBuffer = new Buffer(DEFAULT_BUFLEN);
	std::string userName;
	std::string password;
	int mode;


	while (userName.length() < 4) {
		std::cout << "Please enter your username (must be at least 5 characters): ";

		std::getline(std::cin, userName);
	}
	bool ratingPassed = false;
	int skillRating;
	while (!ratingPassed) {
		std::cout << "Please enter your rating (must be between 1 and 5000): ";
		
		std::cin >> skillRating;
		if (skillRating < 5000 && skillRating>0)
		{
			ratingPassed = true;
		}
	}
	//int skillRating = rand() % 5000 + 1;
	mode = 1;
		//userName = userName + ": ";
	ComposeMessage(userName, skillRating,mode, 1, sendBuffer);
  //  char *sendbuf = "this is a test";
    char recvbuf[DEFAULT_BUFLEN];
    int resultInt;
    int recvbuflen = DEFAULT_BUFLEN;

	//Step 1
    // Initialize Winsock
    resultInt = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (resultInt != 0) 
	{
		std::cout << "WinSock Initalization failed" << std::endl;
		return 1;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    resultInt = getaddrinfo("127.0.0.1", DEFAULT_PORT, &hints, &result);
    if ( resultInt != 0 ) {
        std::cout << "Socket Initalization failed" << std::endl;
		WSACleanup();
        return 1;
    }

	//std::cout << "Step 1: WinSock Initalized" << std::endl;

    // Attempt to connect to an address until one succeeds
    for(ptr=result; ptr != NULL ;ptr=ptr->ai_next) 
	{

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

        if (ConnectSocket == INVALID_SOCKET) 
		{
            std::cout << "Socket failed with error: " << WSAGetLastError() <<std::endl;
            WSACleanup();
            return 1;
        }

        // Connect to server.
        resultInt = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);

        if (resultInt == SOCKET_ERROR) 
		{
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

	//std::cout << "Step 2: Socket Created" << std::endl; //Put this here so we only see the message once althought we may try multiple sockets
	//std::cout << "Step 3: Connected to the server" << std::endl;

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) 
	{
		std::cout << "Unable to connect to server!" << std::endl;
        WSACleanup();
        return 1;
    }

	
    // Send an initial buffer
	//ReadIncomingMessage(sendBuffer);
	const char* temp = sendBuffer->toCharArray();
//	std::cout << sendBuffer->readStringBE(0,sendBuffer->returnBufferSize()) <<std::endl;
	resultInt = send( ConnectSocket, sendBuffer->toCharArray(), sendBuffer->returnBufferSize(), 0 );
    if (resultInt == SOCKET_ERROR) 
	{
		std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }

	//std::cout << "Step 4: Sending / Receiving data" << std::endl;

	//std::cout << "Bytes Sent: " << resultInt << std::endl;


	// shutdown the connection since no more data will be sent

	std::string tempInput;
	mode = 2;
   //  Receive until the peer closes the connection
	//SOCKET ConnectSocket, Buffer* sendBuffer, Buffer* receiveBuffer, char recvbuf[], int recvbuflen
	std::thread receiveThread(ReceiveMessages, ConnectSocket, sendBuffer, receiveBuffer, recvbuf,recvbuflen);
    do 
	{


		sendBuffer->clear();
		std::getline(std::cin, tempInput);
		if (mode == 1)
		{
			mode = 2;
			//ComposeMessage(userName, tempInput,mode, 2, sendBuffer);
			ComposeMessage(userName, skillRating, mode, 1, sendBuffer);
		}
		else 
		{
			mode = 1;
			ComposeMessage(userName, skillRating, mode, 1, sendBuffer);
		}
		//mode = 2;
		//ComposeMessage(userName, tempInput,mode, 2, sendBuffer);
		// ComposeMessage(userName, skillRating, mode, 1, sendBuffer);
		//sendbuf = &tempInput[0u];
		resultInt = send(ConnectSocket, sendBuffer->toCharArray(), sendBuffer->returnBufferSize(), 0);
		if (resultInt == SOCKET_ERROR)
		{
			std::cout << "send failed with error: " << WSAGetLastError() << std::endl;
			closesocket(ConnectSocket);
			WSACleanup();
			return 1;
		}
    } while( resultInt > 0 );

    // cleanup


	//Keep the window open
	//std::cout << "\nwaiting on exit";
	

	resultInt = shutdown(ConnectSocket, SD_SEND);
	if (resultInt == SOCKET_ERROR)
	{
		std::cout << "shutdown failed with error: " << WSAGetLastError() << std::endl;
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}
	closesocket(ConnectSocket);
	WSACleanup();

	//std::cout << "Step 5: Disconnect" << std::endl;
    return 0;
}

void ComposeMessage(std::string userName, int skillRating, int mode, int messageID, Buffer* sendBuffer)
{


		sendBuffer->clear();
		sendBuffer->writeShort16BE(0);
		sendBuffer->setWriteIndex(2);
		sendBuffer->writeIntBE(mode);
		sendBuffer->writeStringBE(userName);
		sendBuffer->writeIntBE(skillRating);
		sendBuffer->setWriteIndex(0);
		sendBuffer->writeShort16BE(sendBuffer->returnBufferSize());
//	}

}



void ReadIncomingMessage(Buffer* receiveBuffer)
{

	int packetlength, messageID, roomID = 0;
	long messageLength = 0;

	std::string userName1;
	std::string userName2;
	std::string ipAddress;





	receiveBuffer->setReadIndex(0);
	//std::string serializedChatPacket = receiveBuffer->readStringBE(52);*/
	packetlength = receiveBuffer->readShort16BE();
	int messageType = receiveBuffer->readIntBE();
	if (messageType == 1)
	{
		int readLength = receiveBuffer->readShort16BE();
		userName1 = receiveBuffer->readStringBE(readLength);
		readLength = receiveBuffer->readShort16BE();
		userName2 = receiveBuffer->readStringBE(readLength);
		readLength = receiveBuffer->readShort16BE();
		ipAddress = receiveBuffer->readStringBE(readLength);

		std::cout << "Player 1: " << userName1 << std::endl;
		std::cout << "Player 2: " << userName2 << std::endl;
		std::cout << "IP Address: " << ipAddress << std::endl;
	}


	receiveBuffer->setReadIndex(0);
	//std::string serializedChatPacket = receiveBuffer->readStringBE(52);*/
	packetlength = receiveBuffer->readShort16BE();
	receiveBuffer->setReadIndex(6);

	std::string serializedChatPacket;
	int messageLen = packetlength - 2;
	serializedChatPacket = receiveBuffer->readStringBE(4, messageLen);//(packetlength -2);


}



void ReceiveMessages(SOCKET ConnectSocket, Buffer* sendBuffer, Buffer* receiveBuffer, char recvbuf[], int recvbuflen)
{
	do
	{
		sendBuffer->setReadIndex(0);
		sendBuffer->setWriteIndex(0);
		int resultInt = 0;
		resultInt = recv(ConnectSocket, recvbuf, recvbuflen, 0);


		if (resultInt > 0)
		{
			receiveBuffer->clear();
			receiveBuffer->loadBuffer(recvbuf, resultInt);

			ReadIncomingMessage(receiveBuffer);
		}
		else if (resultInt == 0)
		{
			//	std::cout << "Connection closed" << std::endl;

		}
		else
			std::cout << "recv failed with error: " << WSAGetLastError() << std::endl;

		
	} while (true);
}
